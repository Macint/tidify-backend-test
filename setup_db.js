var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  
    dbo.createCollection("users", function(err, res) {
    if (err) throw err;
    console.log("Collection Users created!");
    db.close();
  });
  
    dbo.createCollection("tasks", function(err, res) {
    if (err) throw err;
    console.log("Collection Tasks created!");
    db.close();
  });
}); 

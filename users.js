var express = require('express')
var router = express.Router()

//Used for querying by _id
var ObjectId = require('mongodb').ObjectID;

module.exports = router

// GET /
// Returns array of all users
router.get('/', (req, res) => {
    var db = req.db;
    var collection = db.collection('users');
    var query = {};
    collection.find(query).toArray(function(err, result) {
        if (err) throw err;
        res.send(result);
    });
})


// Should be POST /
// Is GET /new now, for testing purposes.
router.get('/new', (req, res) => {
    var new_user = {name: 'test'};
    var db = req.db;
    var collection = db.collection('users');
    var objId;
    collection.insertOne(new_user, function(err, result) {
        if (err) throw err;
        console.log('1 document inserted');
        console.log(result);
        objId = result.insertedId;
    
        // Broadcast via websockets that we have a new user.
        var data = {
          event: 'new_user',
          _id: objId
        }

        req.wss.broadcast(JSON.stringify(data));
        console.log('broadcasted new_user event to websocket clients.');

        res.send({msg: 'Created new user!'})
    });

})

// GET /id
router.get('/:id', (req, res) => {
    var db = req.db;
    var collection = db.collection('users');
    var objectId = new ObjectId(req.params.id);
    var query = {'_id': objectId};
    collection.findOne(query, function(err, result) {
        if (err) throw err;
        res.send({result: result});
    });
})
